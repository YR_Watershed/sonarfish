#' @title Clean Up Mark Data in Consolidated Array
#' @description Extract Date and remove non-data rows
#' @param consolidated_files output from consolidate_files function
#' @importFrom dplyr "%>%"
#' @details Tidies up mark data created by consolidate_files().  Exports
#'   date_time from .id collumn (filename) based on right hand position, filters
#'   out non-data rows. Extracts file length (mins).
#'
#'   Sonar files must be named in the format : “yyyy-mm-dd_hhmmss.aris”.
#'
#'   Datetime is extracted from the filename based on right hand position. Any
#'   file names ending in "(1)" or ".001" are not recognized.
#'
#'   Files with no marks will show file length (mins), and other details as NA
#' @export
tidy_markdata <- function(consolidated_files) {
  #Select date and time string
  datetime <-
    stringr::str_sub(consolidated_files$.id,-26,-10)  ###date hour determined from right hand position


  #Convert datetime string to formatted date and time and set correct time zone
  consolidated_files$date_time <-
    lubridate::ymd_hms(as.POSIXlt(datetime, format = "%Y-%m-%d_%H%M%S"), tz =
                         "America/Whitehorse")

  #Create new data frame dropping .id (filname) and moving date_time name to left side
  x <-
    data.frame (
      consolidated_files %>% dplyr::select(
        date_time,
        sample,
        ping,
        time,
        range,
        amplitude,
        xangle,
        yangle,
        direction,
        length,
        area,
        operator
      )
    )

  #Filter out non-data rows...
  x1 <-
    dplyr::filter(x,!(
      x$sample %in% c("Sample", "Total", "Date", "Start", "Version", "File")
    ))

  #Create a data frame with the lenght each txt file (minutes)
  y1 <- dplyr::filter(x, sample == "Total", ping == "Time")
  y1$mins <- y1$range
  y2 <- as.data.frame(y1 %>% dplyr::select(date_time, mins))

  #Join file lengths to target data
  x2 <- dplyr::right_join(x1, y2, by = c("date_time"))

}
