% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/sonar_VAR2.R
\name{sonar_VAR2}
\alias{sonar_VAR2}
\title{sonar_VAR2}
\usage{
sonar_VAR2(hourData)
}
\arguments{
\item{hourData}{hourly data file output from hr_passage()}
}
\value{
VAR variance
}
\description{
Calculates variance for projects using 2 or fewer sonar ranges
}
\details{
Must add a "Species" collumn to the hourly data in order to work.
  Appropriate for one or two range sonar projects. Equation is explained in
  detail in DFO (2019) and McDougalland Lozori(2018).
}
