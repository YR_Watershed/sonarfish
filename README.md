# sonarfish

<!-- badges: start -->
<!-- badges: end -->

Use to compile marked sonar files (marked with Echotastic 3) into a single folder, enter raw mark data into a single database, estimate and export hourly passage after adjusting for short files, estimate and export passage when data is missing using time-series algorith from imputeTS(), and calculate and export daily passage. Constructed for sonar enumeration programs utilizing one or more sonars, shooting at one or more scan ranges (i,e, Inshore, Offshore). Additionally calculates variance, average percent error, and confidence intervals around counts. 

## Installation

You can install sonarfish from [GitLab](https://CRAN.R-project.org) with:

``` r
install.packages("devtools")
install.packages("remotes")
remotes::install_git("https://gitlab.com/YR_Watershed/sonarfish.git")
```

## Example

This is a basic example:

``` r
## basic example for a sonar program "Fishing Branch" which operates one RB sonar at one scan range

library(sonarfish)

#set working directory (where output files are created)
setwd("C:/Users/milliganm/Desktop/packages")
getwd()

# identify location of marked sonar files
FBR_files <-
  ("D:/RB_ARIS_Files")

#copy files into new subfolder in workign directory
copypaste_files(FBR_files, output_subname = "ARIS_FBR")

#identify location of this new subfolder
FBR_files_new <- (file.path(getwd(), "Processed_Sonar", "ARIS_FBR"))

#consolidate marked files into single database
consol_dat <- consolidate_files(FBR_files_new)

#clean up markdata
mark_dat <- tidy_markdata(consol_dat)

#specify bank ("LB" or "RB") and range(s)
mark_dat_br <- tidy_markdata_br(mark_dat, ranges=1, names="Inshore", bank="RB")

#save mark data
tidy_markdata_save(mark_dat,bank="RB",project_name = "Fishing Branch")

#estimate hourly
FBR_hour <- hr_passage(mark_dat_br, threshold = 20)

#impute missing data and save
FBR_est<- impute_kalman(FBR_hour)
passage_estimate_save(FBR_est, project_name = "Fishing Branch")

#print missing stats
missing_stats(FBR_est)

#summarize daily counts
daily <- daily_est(FBR_est)
daily_estimate_save(daily, project_name = "Fishing Branch")
daily_estimate_plot(daily, project_name = "Fishing Branch")

beepr::beep(8)

```
See the vignette for an example of a sonar project operating two sonars and two scan ranges
